#!/usr/bin/python
"""
launchTotalFullWave2.py

Write simulation files

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def launchTotalFullWave2(c0,omega0,wY,wZ,duration,p0,ppw,cfl,cmapp,rhomap,Amap,boveramap,incoords,outcoords,icmat):
    import numpy as np
    import struct
    from extendMap import extendMap
    from writeCoords import writeCoords
    from writeIC import writeIC
    from writeVabs import writeVabs
    
    nbdy = 40 # number of boundary points for PML
    
    lambda_ = c0/omega0*2*np.pi
    lambda_ = c0/omega0*2*np.pi
    nY = round(wY/lambda_*ppw)+2*nbdy  # number of lateral elements
    nZ = round(wZ/lambda_*ppw)+2*nbdy  # number of depth elements
    nT = round(duration*c0/lambda_*ppw/cfl)

    dY = c0/omega0*2*np.pi/ppw
    dZ = c0/omega0*2*np.pi/ppw
    dT = dY/c0*cfl

    nTic=icmat.shape[1]

    fid = open("c.dat", "wb")
    temp = extendMap(cmapp,nbdy)
    fid.write(struct.pack('%sf' % temp.size, *temp.transpose().reshape(temp.size,1)))
    fid.close()

    fid = open("A.dat", "wb")
    temp = extendMap(Amap,nbdy)
    fid.write(struct.pack('%sf' % temp.size, *temp.transpose().reshape(temp.size,1)))
    fid.close()

    fid = open("rho.dat", "wb")
    temp = extendMap(rhomap,nbdy)
    fid.write(struct.pack('%sf' % temp.size, *temp.transpose().reshape(temp.size,1)))
    fid.close()

    fid = open("N.dat", "wb")
    temp = extendMap((1+boveramap/2)/(rhomap*np.power(cmapp,4)),nbdy)
    fid.write(struct.pack('%sf' % temp.size, *temp.transpose().reshape(temp.size,1)))
    fid.close()

    ncoords = incoords.shape[0]
    ncoordsout = outcoords.shape[0]
    incoords = incoords+nbdy
    outcoords = outcoords+nbdy

    writeCoords('icc.dat',incoords)
    writeCoords('outc.dat',outcoords)
    writeIC('icgen.dat',icmat)

    writeVabs('float',dY,'dY',dZ,'dZ',dT,'dT',c0,'c0')
    writeVabs('int',nY,'nY',nZ,'nZ',nT,'nT',ncoords,'ncoords',ncoordsout,'ncoordsout',nTic,'nTic')
    #outdir = '/';
    #writeVabs('char',outdir,'outdir');

    return (dY, dZ)
