#!/usr/bin/python
"""
extendMap.py

Extend map

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def extendMap(mapp,nbdy):
    #(mapp,nbdy) = (cmapp,nbdy)
    import numpy as np

    map2 = np.zeros((mapp.shape[0]+2*nbdy, mapp.shape[1]+2*nbdy))

    # center 
    map2[nbdy:-nbdy,nbdy:-nbdy] = mapp

    # edges
    for i in range(0, mapp.shape[0]):
        map2[nbdy+i,  :nbdy] = mapp[i, 0]
        map2[nbdy+i,-nbdy: ] = mapp[i,-1]

    for j in range(0, mapp.shape[1]):
        map2[ :nbdy,j+nbdy] = mapp[0,j]
        map2[-nbdy:,j+nbdy] = mapp[-1,j]

    # corners
    map2[ :nbdy,  :nbdy] = mapp[ 0, 0]
    map2[-nbdy:,-nbdy: ] = mapp[-1,-1]
    map2[-nbdy:,  :nbdy] = mapp[-1, 0]
    map2[ :nbdy,-nbdy: ] = mapp[ 0,-1]

    return map2
