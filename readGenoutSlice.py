#!/usr/bin/python
"""
readGenoutslice.py

Read output, in slices of time

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def readGenoutSlice(*arg):
    import numpy as np
    import matplotlib.pyplot as plt
    import struct
    
    plotit = 0

    fname = arg[0]
    nTvec = arg[1]
    ncoordsout = arg[2]
    varargin = arg[3:]
    optargin = len(varargin)
    
    if optargin==1: #stride
        idc = varargin[1]
        genout = np.zeros((len(nTvec),len(idc)))
        fid = open(fname,'rb')    
        for i in range(0,len(nTvec)):
            t = nTvec[i]
            
            fid.seek((t*ncoordsout)*4)
            tmp = struct.unpack('%sf' % ncoordsout, fid.read(4*ncoordsout))
            genout[i,:] = tmp[idc]
            
            print((i+1.0)/len(nTvec))
        fid.close()    
    else: # complete
        genout = np.zeros((len(nTvec),ncoordsout))
        fid = open(fname,'rb')    
        for i in range(0,len(nTvec)):
            t = nTvec[i]
            fid.seek((t*ncoordsout)*4)
            genout[i,:] = struct.unpack('%sf' % ncoordsout, fid.read(4*ncoordsout))
            print((i+1.0)/len(nTvec))
        fid.close()

    if plotit:
        temp=genout[951,:]
        temp2=temp.reshape(modidz.shape[0],modidz.shape[1])
        fig10 = plt.figure()
        ax10  = fig10.add_subplot(111)
        ax10.imshow(temp2, cmap=plt.cm.gray)
        fig10.show()

    return genout

'''
import sys
import time
import struct
import subprocess
import numpy as np #http://scipy-lectures.github.io/intro/numpy/numpy.html
import matplotlib.pyplot as plt
from mapToCoords import mapToCoords
from extendMap import extendMap
from focusCoords import focusCoords
from writeCoords import writeCoords
from writeIC import writeIC
from writeVabs import writeVabs
from launchTotalFullWave2 import launchTotalFullWave2
from sizeOfFile import sizeOfFile
from readGenoutSlice import readGenoutSlice
if sys.version_info[:2] < (2, 7):
    sys.exit("ERROR: Requires Python >= 2.7")

plotit = 0
### Basic variables #########################################
c0 = 1540          # speed of sound (m/s)
omega0 = 2*np.pi*1e6  # center radian frequency of transmitted wave
wY = 2e-2          # width of simulation field (m)
wZ = 3e-2          # depth of simulation field (m)
duration = 40e-6   # duration of simulation (s)
p0 = 1e5           # pressure in Pa
### Advanced variables ######################################
ppw = 15           # number of points per spatial wavelength
cfl = 0.4          # Courant-Friedrichs-Levi condition
### Grid size calculations ##################################
lambda_ = c0/omega0*2*np.pi 
nY = round(wY/lambda_*ppw)   # number of lateral elements
nZ = round(wZ/lambda_*ppw)   # number of depth elements
nT = round(duration*c0/lambda_*ppw/cfl) 
dY = c0/omega0*2*np.pi/ppw
dZ = c0/omega0*2*np.pi/ppw 
dT = dY/c0*cfl 
### Generate field maps ##################################
cmappp = np.ones((nY,nZ))*1540    # speed of sound map (m/s)
rhomap = np.ones((nY,nZ))*1000  # density map (kg/m^3)
Amap = np.ones((nY,nZ))*0.0     # attenuation map (dB/MHz/cm)
boveramap = -2*np.ones((nY,nZ))     # nonlinearity map 
cmappp[round(nY/2)-2:round(nY/2)+1,round(nZ/1.3)-2:round(nZ/1.3)+1]=0.5*c0  # scatterer
if plotit:
    fig1 = plt.figure()
    ax1  = fig1.add_subplot(111)
    ax1.imshow(cmapp.transpose(), cmap=plt.cm.gray)
    fig1.show()

### Generate input coordinates ###########################
inmap = np.zeros((nY,nZ))  
inmap[:,0] = 1
inmap[:,1] = 1
inmap[:,2] = 1
incoords = mapToCoords(inmap)
### Generate initial conditions based on input coordinates ######
ncycles = 2  # number of cycles in pulse
dur = 2  # exponential drop-off of envelope
fcen=[round(nY/2),round(nZ/1.3)]  # center of focus
t = np.arange(nT)/nT*duration-ncycles/omega0*2*np.pi 
icmat=np.zeros((incoords.shape[0],nT))
icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
icmat[incoords.shape[0]/3*0:incoords.shape[0]/3*1,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*0:incoords.shape[0]/3*1,:],icvec,cfl)
t=t-dT/cfl 
icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
if plotit:
    ax4.plot(icvec)
    fig4.show()

icmat[incoords.shape[0]/3*1:incoords.shape[0]/3*2,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*1:incoords.shape[0]/3*2,:],icvec,cfl)
t=t-dT/cfl 
icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
if plotit:
    ax4.plot(icvec)
    fig4.show()

icmat[incoords.shape[0]/3*2:incoords.shape[0]/3*3,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*2:incoords.shape[0]/3*3,:],icvec,cfl)
if plotit:
    fig5 = plt.figure()
    ax5  = fig5.add_subplot(111)
    cax5 = ax5.imshow(icmat.transpose(), cmap=plt.cm.gray)
    ax5.set_title('icmat')
    cbar5 = fig5.colorbar(cax5)
    fig5.show()

### Generate output coordinates ##########################
outmap = np.zeros((nY,nZ)) 
(modidy, modidz) = np.meshgrid(np.arange(0,nY,2), np.arange(0,nZ,2))
outmap[::2, ::2] = 1
outcoords = mapToCoords(outmap) 

### Launch #########################################################
launchTotalFullWave2(c0,omega0,wY,wZ,duration,p0,ppw,cfl,cmappp.transpose(),rhomap.transpose(),Amap.transpose(),boveramap.transpose(),incoords,outcoords,icmat) 
###################################################################
tstart = time.time()
subprocess.call(["./try6_nomex"])
elapsed = time.time() - tstart
print elapsed
###################################################################
ncoordsout=outcoords.shape[0]
nRun=sizeOfFile('genout.dat')/4/ncoordsout
(fname,nTvec,ncoordsout) = ('genout.dat',range(0,nRun),outcoords.shape[0]) 
(fname,nTvec,ncoordsout)
import numpy as np
import sys
import struct
varargin =[]
optargin = len(varargin)
plotit = 1
fname
genout = np.zeros((len(nTvec),ncoordsout))
genout.shape
fid = file(fname,'rb')
for i in range(0,len(nTvec)):
    t = nTvec[i]
    fid.seek((t*ncoordsout)*4)
    genout[i,:] = struct.unpack('%sf' % ncoordsout, fid.read(4*ncoordsout))
    #print "%.2f" % (i+1.0)/len(nTvec)

fid.close()

fig10 = plt.figure()
ax10  = fig10.add_subplot(111)
ax10.imshow(genout, cmap=plt.cm.gray)
fig10.show()

temp=genout[951,:]
temp2=temp.reshape(modidz.shape[0],modidz.shape[1])

fig10 = plt.figure()
ax10  = fig10.add_subplot(111)
ax10.imshow(temp2, cmap=plt.cm.gray)
fig10.show()

'''
