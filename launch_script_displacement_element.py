#!/usr/bin/python
"""
launch_script_displacement_element.py

Launch Fullwave code, easy Python wrapper

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__  = "David Bradway"
__email__   = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def main():
    import sys
    import time
    import subprocess
    import numpy as np #http://scipy-lectures.github.io/intro/numpy/numpy.html
    import matplotlib.pyplot as plt
    import scipy.signal.signaltools as sigtool
    from mapToCoords import mapToCoords
    from focusCoords import focusCoords
    from writeCoords import writeCoords
    from writeVabs import writeVabs
    from launchTotalFullWave2 import launchTotalFullWave2
    from sizeOfFile import sizeOfFile
    from readGenoutSlice import readGenoutSlice
    
    if sys.version_info[:2] < (2, 6):
        sys.exit("ERROR: Requires Python >= 2.6")
    
    plotit = 0

    ### Basic variables #########################################
    c0 = 1540          # speed of sound (m/s)
    omega0 = 2*np.pi*1e6  # center radian frequency of transmitted wave
    wY = 2e-2          # width of simulation field (m)
    wZ = 3e-2          # depth of simulation field (m)
    duration = 40e-6   # duration of simulation (s)
    p0 = 1e5           # pressure in Pa
    ### Advanced variables ######################################
    ppw = 15           # number of points per spatial wavelength
    cfl = 0.4          # Courant-Friedrichs-Levi condition
    ### Grid size calculations ##################################
    lambda_ = c0/omega0*2*np.pi 
    nY = round(wY/lambda_*ppw)   # number of lateral elements
    nZ = round(wZ/lambda_*ppw)   # number of depth elements
    nT = round(duration*c0/lambda_*ppw/cfl) 
    dY = c0/omega0*2*np.pi/ppw 
    dZ = c0/omega0*2*np.pi/ppw 
    dT = dY/c0*cfl 
    ### Generate field maps ##################################
    cmappp = np.ones((nY,nZ))*1540    # speed of sound map (m/s)
    rhomap = np.ones((nY,nZ))*1000  # density map (kg/m^3)
    Amap = np.ones((nY,nZ))*0.0     # attenuation map (dB/MHz/cm)
    boveramap = -2*np.ones((nY,nZ))     # nonlinearity map 
   
    foc=round(nZ/1.3)
    fcen=[round(nY/2.0) foc] # center of focus
    z1=0.95
    z2=0.925
    cmappp[round(nY/2)-2:round(nY/2)+1,foc-1]=z1*c0
    cmappp[round(nY/2)-2:round(nY/2)+1,foc]=z2*c0

    if plotit:
        fig1 = plt.figure()
        ax1  = fig1.add_subplot(111)
        ax1.imshow(cmapp.transpose(), cmap=plt.cm.gray)
        fig1.show()
    
    ### Generate input coordinates ###########################
    inmap = np.zeros((nY,nZ))  
    inmap[:,0] = 1
    inmap[:,1] = 1
    inmap[:,2] = 1
    if plotit:
        fig2 = plt.figure()
        ax2  = fig2.add_subplot(111)
        cax2 = ax2.imshow(inmap.transpose(), cmap=plt.cm.gray)
        ax2.set_title('inmap')
        cbar2 = fig2.colorbar(cax2)
        fig2.show()
    
    incoords = mapToCoords(inmap)
    if plotit:
        fig3 = plt.figure()
        ax3  = fig3.add_subplot(111)
        cax3 = ax3.plot(incoords[:,0],incoords[:,1], 'x')
        ax3.set_title('incoords')
        fig3.show()

    ### Generate output coordinates ##########################
    outmap = np.zeros((nY,nZ)) 

    (modidy, modidz) = np.meshgrid(np.arange(0,nY,2), np.arange(0,nZ,2))
    outmap[::2, ::2] = 1
    if plotit:
        fig6 = plt.figure()
        ax6  = fig6.add_subplot(111)
        cax6 = ax6.imshow(outmap.transpose(), cmap=plt.cm.gray)
        ax6.set_title('outmap')
        cbar6 = fig6.colorbar(cax6)
        fig6.show()

    outcoords = mapToCoords(outmap)
    if plotit:
        fig7 = plt.figure()
        ax7  = fig7.add_subplot(111)
        cax7 = ax7.plot(outcoords[:,0],outcoords[:,1], 'x')
        ax7.set_title('outcoords')
        fig7.show()

    outcoords[:,2] = 1

    outcoords=[outcoords.transpose() [incoords 2*np.ones((incoords.shape[0],1))].transpose()].transpose()
    #outcoords=[outcoords' [incoords 2*ones(size(incoords,1),1)]']'

    ### Generate initial conditions based on input coordinates ######
    ncycles = 2  # number of cycles in pulse
    dur = 2  # exponential drop-off of envelope
    t = np.arange(nT)/nT*duration-ncycles/omega0*2*np.pi 
    fcen=[round(nY/2),round(nZ/1.3)]  # center of focus
    icmat = np.zeros((incoords.shape[0],nT))
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        fig4 = plt.figure()
        ax4  = fig4.add_subplot(111)
        ax4.plot(icvec)
        fig4.show()
    
    icmat[incoords.shape[0]/3*0:incoords.shape[0]/3*1,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*0:incoords.shape[0]/3*1,:],icvec,cfl)
    t=t-dT/cfl 
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        ax4.plot(icvec)
        fig4.show()

    icmat[incoords.shape[0]/3*1:incoords.shape[0]/3*2,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*1:incoords.shape[0]/3*2,:],icvec,cfl)
    t=t-dT/cfl 
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        ax4.plot(icvec)
        fig4.show()

    icmat[incoords.shape[0]/3*2:incoords.shape[0]/3*3,:] = focusCoords(fcen[0],fcen[1],incoords[incoords.shape[0]/3*2:incoords.shape[0]/3*3,:],icvec,cfl)
    if plotit:
        fig5 = plt.figure()
        ax5  = fig5.add_subplot(111)
        cax5 = ax5.imshow(icmat.transpose(), cmap=plt.cm.gray)
        ax5.set_title('icmat')
        cbar5 = fig5.colorbar(cax5)
        fig5.show()

    ### Launch #########################################################
    launchTotalFullWave2(c0,omega0,wY,wZ,duration,p0,ppw,cfl,cmappp.transpose(),rhomap.transpose(),Amap.transpose(),boveramap.transpose(),incoords,outcoords,icmat)
    ###################################################################
    tstart = time.time()
    subprocess.call(["./try6_nomex"])
    elapsed = time.time() - tstart
    print elapsed
    ###################################################################
    ncoordsout=outcoords.shape[0]
    nRun=sizeOfFile('genout.dat')/4/ncoordsout
    genout = readGenoutSlice('genout.dat',range(0,nRun),outcoords.shape[0]) 
    idc = np.where(outcoords[:,2] == 1)[0]
    #idc=find(outcoords(:,3)==1);
    temp = genout[:,idc]
    p1 = temp.reshape(genout.shape[0],modidy.shape[1],modidy.shape[0]) 
    #p1 = reshape(genout(:,idc),size(genout,1),size(modidy,2),size(modidy,1));
    if plotit:
        temp=p1[-1,:,:]
        fig12 = plt.figure()
        ax12  = fig12.add_subplot(111)
        cax12 = ax12.imshow(temp, cmap=plt.cm.gray)
        ax12.set_title('outmap')
        cbar12 = fig12.colorbar(cax12)
        fig12.show()

    idc = np.where(outcoords[:,2] == 2)[0]
    #idc=find(outcoords(:,3)==2);
    pxducer1 = genout[:,idc]
    #pxducer1=genout(:,idc);
    pxducer1 = pxducer1[:,round(pxducer1.shape[1]/3*2):pxducer1.shape[1]]
    #pxducer1=pxducer1(:,round(size(pxducer1,2)/3*2+1:size(pxducer1,2)));
    if plotit:
        fig13 = plt.figure()
        ax13  = fig13.add_subplot(111)
        cax13 = ax13.imshow(np.power(pxducer1,1/4.), cmap=plt.cm.gray)
        ax13.set_title('outmap')
        cbar13 = fig13.colorbar(cax13)
        fig13.show()
    
    #imagesc(powcompress(pxducer1,1/4))

    ###################################################################
    ## MODIFY CMAP AND RELAUNCH ##
    cmappp = np.ones((nY,nZ))*1540    # speed of sound map (m/s)
    cmappp[round(nY/2)-2:round(nY/2)+1,foc-1]=z2*c0
    cmappp[round(nY/2)-2:round(nY/2)+1,foc]=z1*c0

    ### Launch #########################################################
    launchTotalFullWave2(c0,omega0,wY,wZ,duration,p0,ppw,cfl,cmappp.transpose(),rhomap.transpose(),Amap.transpose(),boveramap.transpose(),incoords,outcoords,icmat)
    ###################################################################
    tstart = time.time()
    subprocess.call(["./try6_nomex"])
    elapsed = time.time() - tstart
    print elapsed
    ###################################################################
    ncoordsout=outcoords.shape[0]
    nRun=sizeOfFile('genout.dat')/4/ncoordsout
    genout = readGenoutSlice('genout.dat',range(0,nRun),outcoords.shape[0]) 
    idc = np.where(outcoords[:,2] == 1)[0]
    #idc=find(outcoords(:,3)==1);
    temp = genout[:,idc]
    p2 = temp.reshape(genout.shape[0],modidy.shape[1],modidy.shape[0]) 
    #p2 = reshape(genout(:,idc),size(genout,1),size(modidy,2),size(modidy,1));
    if plotit:
        temp=p2[-1,:,:]
        fig14 = plt.figure()
        ax14  = fig14.add_subplot(111)
        cax14 = ax14.imshow(temp, cmap=plt.cm.gray)
        ax14.set_title('outmap')
        cbar14 = fig14.colorbar(cax14)
        fig14.show()

    idc = np.where(outcoords[:,2] == 2)[0]
    #idc=find(outcoords(:,3)==2);
    pxducer2 = genout[:,idc]
    #pxducer2=genout(:,idc);
    pxducer2 = pxducer2[:,round(pxducer2.shape[1]/3*2):pxducer2.shape[1]]
    #pxducer2=pxducer2(:,round(size(pxducer2,2)/3*2+1:size(pxducer2,2)));
    if plotit:
        fig15 = plt.figure()
        ax15  = fig15.add_subplot(111)
        cax15 = ax15.imshow(np.power(pxducer1,1/4.), cmap=plt.cm.gray)
        ax15.set_title('outmap')
        cbar15 = fig15.colorbar(cax15)
        fig15.show()

    if plotit:
        fig16 = plt.figure()
        ax16  = fig16.add_subplot(111)
        temp = pxducer1[:,round(pxducer1.shape[1]/2)-1]-pxducer2[:,round(pxducer2.shape[1]/2)-1]
        ax16.plot(temp)
        fig16.show()

    #plot(pxducer1(:,round(size(pxducer1,2)/2))-pxducer2(:,round(size(pxducer2,2)/2)))

    px1=pxducer1[:,round(pxducer1.shape[1]/2)-1]
    #px1=pxducer1(:,round(size(pxducer1,2)/2));
    px2=pxducer2[:,round(pxducer2.shape[1]/2)-1]
    #px2=pxducer2(:,round(size(pxducer2,2)/2));

    idt0 = np.argmax(np.abs(sigtool.hilbert(px1)))
    idt = idt0+round(2.*foc*dZ/c0/dT)

    if plotit:
        fig17 = plt.figure()
        ax17  = fig17.add_subplot(111)
        ax17.plot(px1[idt-round(3.*ppw/cfl)-1:idt+round(2.*ppw/cfl)])
        ax17.plot(px2[idt-round(3.*ppw/cfl)-1:idt+round(2.*ppw/cfl)])
        fig17.show()

    if(0):
        r01=(z1-z0)/(z1+z0)
        r12=(z2-z1)/(z2+z1)
        r20=(z0-z2)/(z0+z2)

        r01=(z1-z0)/(z1+z0)
        r12=(z2-z1)/(z2+z1)
        r20=(z0-z2)/(z0+z2)

if __name__ == "__main__":
    main()
