#!/usr/bin/python
"""
focusCoords.py

focus Coordinates

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def focusCoords(idy,idz,coords,icvec,cfl):
    import numpy as np

    dd = np.sqrt(np.power(coords[:,0]-idy,2)+np.power(coords[:,1]-idz,2))
    #dd = -np.round(dd/cfl)
    dd = -myround(dd/cfl)
    dd = dd-np.amin(dd)
    dd = dd.astype(int)

    icmat = np.zeros([coords.shape[0],len(icvec)]) 
    for i in range(0, coords.shape[0]):
        icmat[i,dd[i]:] = icvec[:icvec.shape[0]-dd[i]]

    return icmat


def myround(a, mydecimals=0):
    import numpy as np
    
    return np.around(a+10**(-(mydecimals+8)), decimals=mydecimals)
