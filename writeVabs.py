#!/usr/bin/python
"""
writeVabs.py

write variables of different types

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

def writeVabs (*arg):
    import numpy as np
    import struct
    import sys

    typ = arg[0]
    varargin = arg[1:]

    #print "I was called with", len(arg), "arguments:", arg
    #print "typ has length", len(typ), ":", typ
    #print "varargin has ", len(varargin), "arguments:", varargin

    optargin = len(varargin)
    
    # check even number of arguments
    if optargin % 2 == 1:
        print('ERROR, optargin not even')
        sys.exit(0)
    else:
        for k in range(0,optargin,2):
            fname = varargin[k+1] + '.dat'
            print(str(k/2) + ': Writing ' + fname)
            
            fid = open(fname,'wb')
            if typ == 'float':
                data = struct.pack('f', varargin[k]) # pack float in a binary string
            elif typ == 'int':
                data = struct.pack('i', varargin[k]) # pack integer in a binary string
            
            fid.write(data)
            fid.close()

'''
# For DEBUG:
import numpy as np
import struct
from writeVabs import writeVabs
arg = ('float',dY,'dY',dZ,'dZ',dT,'dT',c0,'c0')
typ = arg[0]
varargin = arg[1:]
optargin = len(varargin)
# check even number of arguments
if optargin % 2 == 1:
    print 'ERROR, optargin not even'
    sys.exit(0)
else:
    for k in range(0,optargin,2):
        k
        fname = varargin[k+1] + '.dat'            
        print 'Writing ' + fname
        
        if typ == 'float':
            format = "f"
        elif typ == 'int':
            format = "i"
        
        fid = file(fname,'wb')
        data = struct.pack(format, varargin[k]) # pack integer in a binary string
        fid.write(data)
        fid.close()

'''
