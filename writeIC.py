#!/usr/bin/python
"""
writeIC.py

write initial condition matrix

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def writeIC (fname,icmat):

    import numpy as np
    import struct

    fid = open(fname, "wb")
    fid.write(struct.pack('%sf' % icmat.size, *icmat.transpose().reshape(icmat.size,1)))
    fid.close()
