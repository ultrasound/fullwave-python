# README

Fullwave-Python library

## DEPENDENCIES

- python-matplotlib
 - CentOS: `sudo yum install python-matplotlib`
 - Ubuntu: `sudo apt-get install python-matplotlib`

## USAGE

- `python launch_script.py`
- `./launch_script`
