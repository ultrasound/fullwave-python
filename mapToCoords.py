#!/usr/bin/python
"""
mapToCoords.py

convert 2D map to coordinate vectors

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

def mapToCoords (inmap):
    import numpy as np
    
    (idx,idy) = np.nonzero(inmap)

    #http://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.view.html
    coords = np.c_[idx,idy]
    coordscopy = coords.copy()
    temp = np.sort(coordscopy.view('i8,i8'), order=['f1','f0'], axis=0)
    tempcopy = temp.copy()
    coordssorted = tempcopy.view(np.int)

    return coordssorted
