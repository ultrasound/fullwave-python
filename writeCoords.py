#!/usr/bin/python
"""
writeCoords.py

write coordinate vectors to file

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = "David Bradway"
__email__ = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def writeCoords(fname,coords):
    
    import numpy as np
    import struct
    
    fid = open(fname,'wb')    
    a=coords.transpose().reshape(coords.size,1).astype(int)
    a=[x[0] for x in a]
    fid.write(struct.pack('%si' % coords.size, *a))
    fid.close()
