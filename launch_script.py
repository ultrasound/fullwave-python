#!/usr/bin/python
"""
launch_script.py

Launch Fullwave code, easy Python wrapper

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__  = "David Bradway"
__email__   = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def main():
    import sys
    import time
    import subprocess
    import numpy as np #http://scipy-lectures.github.io/intro/numpy/numpy.html
    import matplotlib.pyplot as plt
    from mapToCoords import mapToCoords
    from focusCoords import focusCoords
    from writeCoords import writeCoords
    from writeVabs import writeVabs
    from launchTotalFullWave2 import launchTotalFullWave2
    from sizeOfFile import sizeOfFile
    from readGenoutSlice import readGenoutSlice
    
    if sys.version_info[:2] < (2, 6):
        sys.exit("ERROR: Requires Python >= 2.6")
    
    plotit = 1

    ### Basic variables #########################################
    c0 = 1540          # speed of sound (m/s)
    omega0 = 2*np.pi*1e6  # center radian frequency of transmitted wave
    wY = 2e-2          # width of simulation field (m)
    wZ = 3e-2          # depth of simulation field (m)
    duration = 40e-6   # duration of simulation (s)
    p0 = 1e5           # pressure in Pa
    ### Advanced variables ######################################
    ppw = 15           # number of points per spatial wavelength
    cfl = 0.4          # Courant-Friedrichs-Levi condition
    ### Grid size calculations ##################################
    lambda_ = c0/omega0*2*np.pi 
    nY = round(wY/lambda_*ppw)   # number of lateral elements
    nZ = round(wZ/lambda_*ppw)   # number of depth elements
    nT = round(duration*c0/lambda_*ppw/cfl) 
    dY = c0/omega0*2*np.pi/ppw 
    dZ = c0/omega0*2*np.pi/ppw 
    dT = dY/c0*cfl 
    ### Generate field maps ##################################
    cmap = np.ones((nY,nZ))*1540    # speed of sound map (m/s)
    rhomap = np.ones((nY,nZ))*1000  # density map (kg/m^3)
    Amap = np.ones((nY,nZ))*0.0     # attenuation map (dB/MHz/cm)
    boveramap = -2*np.ones((nY,nZ))     # nonlinearity map 
    cmap[round(nY/2)-2:round(nY/2)+1,round(nZ/1.3)-2:round(nZ/1.3)+1]=0.5*c0  # scatterer
    
    if plotit:
        fig1 = plt.figure()
        ax1  = fig1.add_subplot(111)
        ax1.imshow(cmap.transpose(), cmap=plt.cm.gray)
        fig1.show()
    
    ### Generate input coordinates ###########################
    inmap = np.zeros((nY,nZ))  
    inmap[:,0] = 1
    inmap[:,1] = 1
    inmap[:,2] = 1
    if plotit:
        fig2 = plt.figure()
        ax2  = fig2.add_subplot(111)
        cax2 = ax2.imshow(inmap.transpose(), cmap=plt.cm.gray)
        ax2.set_title('inmap')
        cbar2 = fig2.colorbar(cax2)
        fig2.show()
    
    incoords = mapToCoords(inmap)
    if plotit:
        fig3 = plt.figure()
        ax3  = fig3.add_subplot(111)
        cax3 = ax3.plot(incoords[:,0],incoords[:,1], 'x')
        ax3.set_title('incoords')
        fig3.show()
    
    ### Generate initial conditions based on input coordinates ######
    ncycles = 2  # number of cycles in pulse
    dur = 2  # exponential drop-off of envelope
    fcen=[round(nY/2),round(nZ/1.3)]  # center of focus
    t = np.arange(nT)/nT*duration-ncycles/omega0*2*np.pi 
    
    icmat=np.zeros((incoords.shape[0],nT))
    
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        fig4 = plt.figure()
        ax4  = fig4.add_subplot(111)
        ax4.plot(icvec)
        fig4.show()
    
    s=int(incoords.shape[0]/3)
    icmat[0:s,:] = focusCoords(fcen[0],fcen[1],incoords[0:s,:],icvec,cfl)

    t=t-dT/cfl 
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        ax4.plot(icvec)
        fig4.show()

    icmat[s:s*2,:] = focusCoords(fcen[0],fcen[1],incoords[s:s*2,:],icvec,cfl)


    t=t-dT/cfl 
    icvec = np.exp(-np.power(1.05*t*omega0/(ncycles*np.pi), 2*dur))*np.sin(t*omega0)*p0
    if plotit:
        ax4.plot(icvec)
        fig4.show()

    icmat[s*2:s*3,:] = focusCoords(fcen[0],fcen[1],incoords[s*2:s*3,:],icvec,cfl)

    if plotit:
        fig5 = plt.figure()
        ax5  = fig5.add_subplot(111)
        cax5 = ax5.imshow(icmat.transpose(), cmap=plt.cm.gray)
        ax5.set_title('icmat')
        cbar5 = fig5.colorbar(cax5)
        fig5.show()

    ### Generate output coordinates ##########################
    outmap = np.zeros((nY,nZ)) 

    (modidy, modidz) = np.meshgrid(np.arange(0,nY,2), np.arange(0,nZ,2))
    outmap[::2, ::2] = 1
    if plotit:
        fig6 = plt.figure()
        ax6  = fig6.add_subplot(111)
        cax6 = ax6.imshow(outmap.transpose(), cmap=plt.cm.gray)
        ax6.set_title('outmap')
        cbar6 = fig6.colorbar(cax6)
        fig6.show()

    outcoords = mapToCoords(outmap)
    if plotit:
        fig7 = plt.figure()
        ax7  = fig7.add_subplot(111)
        cax7 = ax7.plot(outcoords[:,0],outcoords[:,1], 'x')
        ax7.set_title('outcoords')
        fig7.show()

    ### Launch #########################################################
    launchTotalFullWave2(c0,omega0,wY,wZ,duration,p0,ppw,cfl,cmap.transpose(),rhomap.transpose(),Amap.transpose(),boveramap.transpose(),incoords,outcoords,icmat)
    ###################################################################
    tstart = time.time()
    subprocess.call(["./try6_nomex"])
    elapsed = time.time() - tstart
    print(elapsed)
    ###################################################################
    ncoordsout=outcoords.shape[0]
    nRun=int(sizeOfFile('genout.dat')/4/ncoordsout)
    genout = readGenoutSlice('genout.dat',range(0,nRun),outcoords.shape[0]) 
    p = genout.reshape(genout.shape[0],modidy.shape[0],modidy.shape[1]) 
    
    if plotit:
        temp=p[-1,:,:]
        fig12 = plt.figure()
        ax12  = fig12.add_subplot(111)
        cax12 = ax12.imshow(temp, cmap=plt.cm.gray)
        ax12.set_title('outmap')
        cbar12 = fig12.colorbar(cax12)
        fig12.show()
    return p

    '''
    fig13 = plt.figure()
    ax13  = fig13.add_subplot(111)
    ax13.set_title('outmap')
    for i in range(1,p.shape[0],5):
        temp=p[i,:,:]
        cax13 = ax13.imshow(temp, cmap=plt.cm.gray)
        fig13.show()
        time.sleep(.1) # delays for 5 seconds

    '''

if __name__ == "__main__":
    main()
