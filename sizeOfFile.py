#!/usr/bin/python
"""
sizeOfFile.py

Determine the size of a file

=======
Copyright 2014 David P. Bradway (dpb6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__  = "David Bradway"
__email__   = "dpb6@duke.edu"
__license__ = "Apache v2.0"


def sizeOfFile(fname):
    fid = open(fname,'rb')
    fid.seek(0,2)
    filesize = fid.tell()
    fid.close()
    return filesize

"""
# Test: 409200 Oct 12 18:45 rho.dat
fname='rho.dat'
fid = file(fname,'rb')
fid.seek(0,2)
filesize = fid.tell()
fid.close()
filesize
#409200
"""
